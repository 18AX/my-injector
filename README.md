# my-injector
Little shared object injector using `__libc_dlopen_mode` from the libc. It's only works for x86_64 Linux systemsk.
## How to use it
```bash
make
make lib # compile the lib example
./injector <pid> <full path shared object>