#include <dlfcn.h>
#include <err.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(void)
{
    pid_t pid = getpid();

    printf("pid=%d\n", pid);

    char *libc_address = (char *)dlopen("libc-2.28.so", RTLD_NOW);
    char *dl_open_address = (char *)dlsym(libc_address, "__libc_dlopen_mode");
    char *malloc_address = (char *)dlsym(libc_address, "malloc");

    long long offset = dl_open_address - libc_address;
    printf("libc: %p\n", (void *)libc_address);
    printf("malloc: %p\n", (void *)malloc_address);

    printf("offeset = %lld dl_open_address = %p\n\n", offset,
           (void *)dl_open_address);

    getchar();
}