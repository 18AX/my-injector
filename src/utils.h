#ifndef UTILS_HEADER
#define UTILS_HEADER

#include <stddef.h>

struct mapped_area
{
    void *low_address;
    void *high_address;
    char *perms;
    char *path;
};

struct mapped_area *parse_proc_map_line(char *line);

void free_area(struct mapped_area *area);

void target_write(int pid, long *address, long *data, size_t size);

void target_read(int pid, long *address, long *data, size_t size);

#endif
