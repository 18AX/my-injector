#ifndef INJECTOR_HEADER
#define INJECTOR_HEADER

#define _GNU_SOURCE

#include <sys/user.h>

void inject(int pid, void *target_libc, void *dtaget_dl_address,
            void *target_malloc, struct user_regs_struct saved_regs,
            char *dl_name);

#endif
