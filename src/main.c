#define _GNU_SOURCE
#include <dlfcn.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <unistd.h>

#include "injector.h"
#include "utils.h"

static struct mapped_area *get_libc_address(size_t pid)
{
    char buffer[1024];

    sprintf(buffer, "/proc/%zu/maps", pid);
    FILE *file = fopen(buffer, "r");

    if (file == NULL)
    {
        err(1, "Cannot open");
    }

    char *line = NULL;
    size_t n;

    while (getline(&line, &n, file) > 0)
    {
        struct mapped_area *map = parse_proc_map_line(line);

        if (strstr(map->path, "libc") != NULL
            && strstr(map->perms, "x") != NULL)
        {
            free(line);
            return map;
        }

        free_area(map);
    }

    free(line);

    return NULL;
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        errx(1, "Usage: ./injector <pid> <shared object>");
    }

    size_t pid = atoi(argv[1]);

    long ret = ptrace(PTRACE_ATTACH, pid, NULL, NULL);

    if (ret == -1)
    {
        err(2, "ptrace error:");
    }

    wait(NULL);

    printf("[+] attached to %zu\n", pid);

    // We need to save registers to restore the execution of the program
    struct user_regs_struct saved_regs;

    ret = ptrace(PTRACE_GETREGS, pid, NULL, &saved_regs);

    if (ret == -1)
    {
        err(2, "cannot read registers");
    }

    // struct user_regs_struct new_regs = saved_regs;

    struct mapped_area *process_libc = get_libc_address(pid);
    if (process_libc == NULL)
    {
        errx(2, "cannot find process libc");
    }

    // find target libc base address
    char *libc_address = (char *)dlopen(process_libc->path, RTLD_NOW);

    if (libc_address == NULL)
    {
        errx(1, "open libc: %s", dlerror());
    }

    char *dlopen_address =
        (char *)dlsym((void *)libc_address, "__libc_dlopen_mode");

    char *malloc_address = (char *)dlsym((void *)libc_address, "malloc");

    int self_pid = getpid();

    struct mapped_area *self_libc = get_libc_address(self_pid);

    // Calculate offset between the begining of the libc and the function (it
    // will be the same on our libc and the target)
    long long dl_offset = dlopen_address - ((char *)self_libc->low_address);

    long long malloc_offset = malloc_address - ((char *)self_libc->low_address);

    // Calculate target function address
    char *target_dl_open = ((char *)process_libc->low_address) + dl_offset;

    char *target_malloc = ((char *)process_libc->low_address) + malloc_offset;

    printf("[+] target __libc_dlopen_mode at %p\n", (void *)target_dl_open);
    printf("[+] malloc at %p\n", (void *)target_malloc);

    inject(pid, process_libc->low_address, target_dl_open, target_malloc,
           saved_regs, argv[2]);

    printf("[+] shared library injected\n");

    free_area(process_libc);
    free_area(self_libc);

    ptrace(PTRACE_DETACH, pid, NULL, NULL);
}
