#define _GNU_SOURCE

#include "injector.h"

#include <err.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <unistd.h>

#include "utils.h"

#define BUFFER_SIZE 1024

/**
 * name is on rdi, mode on rsi and dl_open_mode address on rdx
 */
static void stub()
{
    __asm__("callq *%rsi \n \n"
            "int $3 \n"
            "call *%rdx \n"
            "int $3 \n");
}

static void end()
{}

static size_t get_size(size_t size)
{
    return (size & ~(sizeof(long) - 1)) + sizeof(long);
}

void inject(int pid, void *target_libc, void *target_dl_address,
            void *target_malloc, struct user_regs_struct saved_regs,
            char *dl_name)
{
    long saved[BUFFER_SIZE];

    long *addr = target_libc;
    addr += 1;

    // Save original code
    target_read(pid, addr, saved, BUFFER_SIZE);

    // Copy stub code
    char *stub_addr = (char *)stub;
    char *stub_end_addr = (char *)end;

    size_t stub_size = stub_end_addr - stub_addr;

    char *stub_buffer = malloc(stub_size * sizeof(char));

    memset(stub_buffer, 0, stub_size);

    memcpy(stub_buffer, stub_addr, stub_size);

    target_write(pid, addr, (long *)stub_buffer, stub_size / sizeof(long));

    free(stub_buffer);

    // First call

    size_t path_len = strlen(dl_name);
    size_t to_allocate = get_size(path_len) * sizeof(char);

    struct user_regs_struct new_regs = saved_regs;

    new_regs.rdi = (long)to_allocate;
    new_regs.rsi = (long)target_malloc;
    new_regs.rip = (long)(((char *)addr) + 2);

    if (ptrace(PTRACE_SETREGS, pid, NULL, &new_regs) == -1)
    {
        err(2, "ptrace set regs");
    }

    ptrace(PTRACE_CONT, pid, NULL, NULL);

    int status;

    // Waiting for the target to hit the second interupt

    waitpid(pid, &status, WUNTRACED);

    siginfo_t sig;

    if ((ptrace(PTRACE_GETSIGINFO, pid, NULL, &sig)) == -1)
    {
        err(2, "Cannot get siginfo");
    }

    if (sig.si_signo != SIGTRAP)
    {
        errx(2, "Cannot find sigtrap %d", sig.si_signo);
    }

    ptrace(PTRACE_GETREGS, pid, NULL, &new_regs);

    // malloc result
    long *malloc_ret = (long *)new_regs.rax;

    char *path_buffer = malloc(to_allocate);
    memset(path_buffer, 0, to_allocate / sizeof(char));
    memcpy(path_buffer, dl_name, path_len);

    // Write the dl path to the allocated memory into the target
    target_write(pid, malloc_ret, (long *)path_buffer,
                 to_allocate / sizeof(long));

    // Sets the arguments for the __libc_dlopen_mode
    new_regs.rdi = (long)malloc_ret;
    new_regs.rsi = (long)0x1;
    new_regs.rdx = (long)target_dl_address;

    if (ptrace(PTRACE_SETREGS, pid, NULL, &new_regs) == -1)
    {
        err(2, "ptrace set regs");
    }

    ptrace(PTRACE_CONT, pid, NULL, NULL);

    // Wait for the target to hit the last interupt
    waitpid(pid, &status, WUNTRACED);

    if ((ptrace(PTRACE_GETSIGINFO, pid, NULL, &sig)) == -1)
    {
        err(2, "Cannot get siginfo");
    }

    if (sig.si_signo != SIGTRAP)
    {
        errx(2, "Cannot find sigtrap %d", sig.si_signo);
    }

    // restore registers
    ptrace(PTRACE_SETREGS, pid, NULL, &saved_regs);
}
