#define _GNU_SOURCE
#include "utils.h"

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>

static char *skip_elements(char *ptr, char delimiter)
{
    while (*ptr != delimiter && *ptr != '\0')
    {
        ptr++;
    }

    return ptr;
}

struct mapped_area *parse_proc_map_line(char *s)
{
    char *line = strdup(s);
    struct mapped_area *map = malloc(sizeof(struct mapped_area));

    char *begin = line;
    char *end = skip_elements(begin, '-');
    ;

    *end = '\0';

    map->low_address = (void *)strtol(begin, NULL, 16);
    *end = '-';

    begin = end + 1;

    end = skip_elements(begin, ' ');
    *end = '\0';

    map->high_address = (void *)strtol(begin, NULL, 16);
    *end = ' ';

    begin = end + 1;

    end = skip_elements(begin, ' ');
    *end = '\0';

    map->perms = strdup(begin);
    *end = ' ';

    end = skip_elements(line, '\0');

    begin = end;

    while (*begin != ' ' && *begin != '\t')
    {
        begin--;
    }

    if (*(end - 1) == '\n')
    {
        *(end - 1) = '\0';
    }

    map->path = strdup(begin + 1);

    free(line);
    return map;
}

void free_area(struct mapped_area *area)
{
    if (area == NULL)
    {
        return;
    }

    free(area->perms);
    free(area->path);

    free(area);
}

void target_write(int pid, long *address, long *data, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        if (ptrace(PTRACE_POKEDATA, pid, address + i, data[i]) == -1)
        {
            err(2, "ptrace write");
        }
    }
}

void target_read(int pid, long *address, long *data, size_t size)
{
    for (size_t i = 0; i < size; ++i)
    {
        if ((data[i] = ptrace(PTRACE_PEEKDATA, pid, address + i, NULL)) == -1)
        {
            err(2, "ptrace write");
        }
    }
}
