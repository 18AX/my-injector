CC=clang-11
LFLAGS= -ldl
CFLAGS=-Wall -pedantic -std=c99 -g3

OBJ=main.o utils.o injector.o
TARGET=injector

.PHONY: all clean

$(TARGET): $(OBJ)
	$(CC) $(LFLAGS) $(OBJ) -o $(TARGET)

lib: CFLAGS = -Werror -Wall -pedantic -std=c99 -shared -fPIC
lib: tests/lib
	$(CC) $(CFLAGS) -o libexample.so tests/lib.c

%.o: src/%.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) *.o $(TARGET) libexample.so
